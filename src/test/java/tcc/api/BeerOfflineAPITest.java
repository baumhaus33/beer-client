package tcc.api;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import java.util.List;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import tcc.BeerPersitanceManager;
import tcc.api.BeerOfflineApi;
import tcc.entities.Beer;

/**
 *
 * @author christian
 */
public class BeerOfflineAPITest {

    private static final String BEER_NAME = "B";

    @BeforeClass
    public static void setUpClass() throws IOException {
        BeerPersitanceManager b = new BeerPersitanceManager();
        b.sync();
    }

    @Test
    public void test() throws IOException {
        BeerOfflineApi beerOfflineApi = new BeerOfflineApi();

        // do the test 20 times for with pagesize 1 to 20.
        for (int pageSize = 1; pageSize <= 20; pageSize++) {
            List<Beer> findBeersByName;
            int page = 1;
            do {
                findBeersByName = beerOfflineApi.findBeersByName(BEER_NAME, pageSize, page);
                System.out.println("found " + findBeersByName.size() + " in page " + page );
                assertNotNull(findBeersByName);
                page++;
                assertTrue(findBeersByName.size() <= pageSize);
            } while (!findBeersByName.isEmpty()); // find all

        }
    }
}
