/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tcc;

import java.util.List;
import tcc.entities.Beer;
import tcc.entities.Ingredient;

/**
 * Contains tools to present present data to the user and get data from the
 * input.
 *
 * @author christian
 */
public class BeerCmdUtil {

    public static void showBeer(final Beer beer) {
        System.out.println("\n\nname: " + beer.getName());
        System.out.println("description:  " + beer.getDescription());
        System.out.println("hopes");
        for (Ingredient hop : beer.getIngredients().getHops()) {
            System.out.println("\t" + hop);
        }
        System.out.println("malt");
        for (Ingredient malt : beer.getIngredients().getMalt()) {
            System.out.println("\t" + malt);
        }

        System.out.println("\nyeast: " + beer.getIngredients().getYeast());
    }

    public static void showBeers(final List<Beer> show) {

        for (Beer beer : show) {
            showBeer(beer);
        }
    }

    /**
     * Prints a beer List to the command Line.
     *
     * @param beerPage the beers to show.
     */
    public static void presentBeerList(List<Beer> beerPage) {
        if (beerPage.isEmpty()) {
            System.out.println("no beers found");
        } else {
            BeerCmdUtil.showBeers(beerPage);

            System.out.println(Integer.toString(beerPage.size()) + " beers found");
        }
    }

}
