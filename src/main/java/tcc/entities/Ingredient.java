/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tcc.entities;

import tcc.entities.Amount;
import java.io.Serializable;
import lombok.Getter;
import lombok.Setter;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 *
 * @author christian
 */
@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class Ingredient implements Serializable {

    private static final long serialVersionUID = 1L;

    private Amount amount;
    private String name;

    @Override
    public String toString() {
        return amount.toString() + " " + name;
    }

}
