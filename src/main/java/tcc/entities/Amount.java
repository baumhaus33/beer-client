/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tcc.entities;

import java.io.Serializable;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author christian
 */
@Getter
@Setter
public class Amount implements Serializable {

    private static final long serialVersionUID = 1L;

    private float value;
    private String unit;

    @Override
    public String toString() {
        return Float.toString(value) + " " + unit;
    }

}
