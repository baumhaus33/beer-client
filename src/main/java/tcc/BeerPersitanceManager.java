/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tcc;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import javax.net.ssl.HttpsURLConnection;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import tcc.api.BeerOnlineConnetor;
import tcc.entities.Beer;

/**
 * Class to save the beers local in a json file.
 *
 * @author christian
 */
public class BeerPersitanceManager {

    // Must be a number greater than 0 and less than 80
    private static final int PAGE_SIZE = 79;
    public static final File PERSISTANCE_FILE = new File(System.getenv("HOME") + File.separator + "beerPeristance.json");

    private final BeerOnlineConnetor b = new BeerOnlineConnetor();
    private final ObjectMapper mapper = new ObjectMapper();

    /**
     * Stores all Beers from the online api in a local file. If this function
     * fails the peristance file gets deleted so there is no currupt file left.
     *
     * @throws java.io.FileNotFoundException
     * @throws MalformedURLException
     * @throws IOException
     */
    public void sync() throws FileNotFoundException, IOException {

        List<Beer> readValue;
        int page = 1;
        boolean first = true;
        try (PrintWriter writer = new PrintWriter(PERSISTANCE_FILE)) {
            // start of json array
            writer.print("[");
            do {

                String createStringURL = b.createStringURL(PAGE_SIZE, page);
                HttpsURLConnection con = b.getConnection(new URL(createStringURL));

                // convert to Object and back to get rid off unnedded files
                try (InputStream in = con.getInputStream()) {
                    readValue = mapper.readValue(in, new TypeReference<List<Beer>>() {
                    });

                    for (Beer beer : readValue) {
                        String writeValueAsString = mapper.writeValueAsString(beer);
                        if (first) {
                            first = false;
                        } else {
                            writer.print(",");
                        }
                        writer.println(writeValueAsString);
                    }
                }
                page++;
                con.disconnect();

            } while (!readValue.isEmpty());

            // end of json array
            writer.print("]");
            writer.flush();
        } catch (IOException e) {
            PERSISTANCE_FILE.delete();
            throw e;
        }

    }

}
